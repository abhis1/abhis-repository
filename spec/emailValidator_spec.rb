require 'EmailValidator'
require 'rspec'


describe 'EmailValidator' do
  
  let(:validator) { EmailValidator.new }
    it 'given wrong format of email, return false' do
       expect(validator.is_email_valid?("a.a")).to be(false)
    end


    it { expect(validator.is_email_valid?("a@b.a")).to be(true) }
end
