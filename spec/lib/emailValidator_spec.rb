require 'EmailValidator'
require 'rspec'


describe 'EmailValidator' do
  let(:validator) { EmailValidator.new }
    it { expect(validator.is_email_valid?("a.a")).to be(false)  }
    it { expect(validator.is_email_valid?("a@b.a")).to be(true) }
end
