require 'CarPremiumCalculator'
require 'rspec'
require 'CarQuote'

describe CarPremiumCalculator do
 describe 'it accounts for gender when calculating a premium' do
   let(:quote) { CarQuote.new(25, "a@a.a", "nsw", "audi", gender, 1990)}

   context 'for a male' do
     let(:gender) { 'male' }

     it 'should calculate the correct premium' do
       expect(subject.get_premium_for_quote(quote)).to eq("51.25")
     end
   end

   context 'for a female' do
     let(:gender) { 'female' }

     it 'should calculate the correct premium' do
       expect(subject.get_premium_for_quote(quote)).to eq("47.95")
     end
   end
 end
end
